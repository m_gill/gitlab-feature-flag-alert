require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::Issue do
	let(:group) { "group::code review" }
	let(:flags) {
		[
			Gitlab::FeatureFlagAlert::FeatureFlag.new({
				"name" => "flag_one",
				"introduced_by_url" => nil,
				"rollout_issue_url" => nil,
				"milestone" => "12.0",
				"type" => "development",
				"group" => "group::code review",
				"default_enabled" => false
			}),
			Gitlab::FeatureFlagAlert::FeatureFlag.new({
				"name" => "flag_two",
				"introduced_by_url" => nil,
				"rollout_issue_url" => "http://nope.com",
				"milestone" => "12.0",
				"type" => "development",
				"group" => "group::code review",
				"default_enabled" => true
			})
		]
	}
	subject { Gitlab::FeatureFlagAlert::Issue.new(group, flags, nil) }

	describe "#prepared_flags" do
		it "creates checkboxes for each flag" do
			expectation = ["- [ ] `flag_one` - %12.0 *Missing rollout issue", "- [ ] `flag_two` - %12.0 http://nope.com"]

			expect(subject.prepared_flags).to eq(expectation)
		end
	end

	describe "#issue_body" do
		it "returns a bunch of text" do
			expectation = <<~STRING
This is a group level feature flag report containing feature flags that have been enabled in the codebase for 2 or more releases.

Please review these feature flags to determine if they are able to be removed entirely.

Feature flag trends can be found in the [Sisense dashboard.](https://app.periscopedata.com/app/gitlab/792066/Engineering-::-Feature-Flags)

----

- [ ] `flag_one` - %12.0 *Missing rollout issue
- [ ] `flag_two` - %12.0 http://nope.com

----

/label ~"group::code review"
/assign @m_gill
			STRING

			expect(subject.body).to eq(expectation)
		end
	end
	
	describe "#title" do
		it "includes group and date" do
			expect(subject.title).to include(Date.today.iso8601)
			expect(subject.title).to include("group::code review")
		end
	end
end
