require "spec_helper"

RSpec.describe Gitlab::FeatureFlagAlert::GitlabService do
  let(:issue) { Gitlab::FeatureFlagAlert::Issue.new("group::code review", [], nil) }

  describe "#retrieve_assignees" do
    context "when error" do
      before do
        stub_request(:get, "https://gitlab.com/gitlab-org/quality/triage-ops/-/raw/master/group-definition.yml")
          .with(query: hash_including("PRIVATE-TOKEN"))
          .to_return(status: [404, "Not Found"])
      end

      it "returns error" do
        expect { subject.retrieve_assignees }.to raise_error(Gitlab::FeatureFlagAlert::Error)
      end
    end

    context "when successful" do
      it "returns YAML" do
        stub_request(:get, "https://gitlab.com/gitlab-org/quality/triage-ops/-/raw/master/group-definition.yml")
          .with(query: hash_including("PRIVATE-TOKEN"))
          .to_return(status: [200], body: "---\n# Comment/\ngroup:\n  backend_engineering_manager:\n  - \"@username\"\n")


        expect(subject.retrieve_assignees).to eq({"group"=>{"backend_engineering_manager"=>["@username"]}})
      end
    end
  end

  describe "#create_issue" do
    context "when error" do
      before do
        stub_request(:post, "https://gitlab.com/api/v4/projects/#{ENV['TRIAGE_PROJECT_ID']}/issues")
          .to_return(status: [500, "Internal Server Error"])
      end

      it "returns error" do
        expect { subject.create_issue(issue) }.to raise_error(Gitlab::FeatureFlagAlert::Error)
      end
    end

    context "when successful" do
      it "returns successful" do
        stub_request(:post, "https://gitlab.com/api/v4/projects/#{ENV['TRIAGE_PROJECT_ID']}/issues")
          .to_return(status: 200, body: "{}")

        expect(subject.create_issue(issue)).to eq({})
      end
    end
  end
end

